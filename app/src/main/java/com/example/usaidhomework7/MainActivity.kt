package com.example.usaidhomework7

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.InputType
import android.widget.Button
import android.widget.EditText
import androidx.core.content.ContextCompat
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var passwordEyeButton: Button
    private lateinit var passwordEditText: EditText
    var passwordEye: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        passwordEyeButton = findViewById(R.id.invisiblepassword)
        passwordEditText = findViewById(R.id.passwordEditText)
        init()

    }

    private fun init() {

        invisiblepassword.setOnClickListener {
            if (passwordEye == false) {
                passwordEyeButton.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_component_1___11))
                passwordEditText.inputType = InputType.TYPE_CLASS_TEXT
                passwordEye = !passwordEye
            }else {
                passwordEyeButton.setBackground(ContextCompat.getDrawable(this, R.drawable.ic_component_1___12))
                passwordEditText.inputType = InputType.TYPE_TEXT_VARIATION_PASSWORD
                passwordEye = !passwordEye
            }
        }



    }

}